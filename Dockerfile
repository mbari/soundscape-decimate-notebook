FROM jupyter/base-notebook

MAINTAINER Danelle Cline <dcline@mbari.org>

USER root
RUN apt-get -qq update && \
    apt-get install -y libsndfile1 && \
    apt-get install -y gcc && \
    apt-get install -y libatlas-base-dev

#########################################################################
# Uncomment to include PDF export support (makes Docker image larger!)
#RUN apt-get install -y texlive-xetex pandoc
#########################################################################
USER $NB_UID
ADD src .
RUN pip install Cython numpy
ADD requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt
