[MBARI](https://www.mbari.org/wp-content/uploads/2014/11/logo-mbari-3b.png)
# MBARI Soundscape Project
Jupyter notebook to demonstrate the decimation filtering used in the MBARI Soundscape Project  

# System Requirements
* [Docker](https://www.docker.com/community-edition#/download)(https://www.docker.com/community-edition#/download)

## Create docker images
```bash
docker build -t soundscape-decimate-notebook .
```

## Run Docker image  
```bash
docker run -p 8888:8888 soundscape-decimate-notebook
```
when this command is done, you will see a long URL at the bottom, e.g. 

```bash
 Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://localhost:8888/?token=1b1f538cdd2897eb1e479fa871188ed75312df8d6c1df251
```

Copy and paste that URL into your favorite browser. You should see a notebook similar to [decimate.pdf](decimate.pdf).

## Developer notes

It can be built on any platform that supports Docker, e.g. Windows, Mac, Linux. 
     
## 1. Build a Docker image

Open up a terminal window in the same directory you checked out the code to and run the following. This will build and tag the docker container with the name *soundscape-decimate-notebook*.  
```bash
docker build -t soundscape-decimate-notebook .
```

## 2. Run the Docker image
This will run the notebook in read-only mode. You will be able to make changes and download them to your computer.
```bash
docker run -it --rm -p 8888:8888 soundscape-decimate-notebook start-notebook.sh --NotebookApp.token=''
```
Open a web browser to the notebook [http://localhost:8888/notebooks/work/decimate.ipynb](http://localhost:8888/notebooks/work/decimate.ipynb)

## 2. (Optional) Run the Docker image saving changes to the local host
To edit and save changes to the local host, run the docker image with passwordless login, sharing the directory *work* between the host and container. 
This will allow you to retain any changes to the notebook in your local *work* directory.
You should only enable sudo if you trust the user and/or if the container is running on an isolated host. 
```bash
docker run -it -e GRANT_SUDO=yes --user root --rm -p 8888:8888 -v $PWD/work:/home/jovyan/work soundscape-decimate-notebook start-notebook.sh  --NotebookApp.token=''
```
Open a web browser to the notebook [http://localhost:8888/notebooks/work/decimate.ipynb](http://localhost:8888/notebooks/work/decimate.ipynb)
